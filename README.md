	
## Lectures/Practicals notes, Code, Data and Solutions for the Computational Methods in Ecology and Evolution modules/courses at Imperial College London

### FOR STUDENTS: 
YOU WILL ONLY HAVE READ PERMISSION TO THIS REPOSITORY. PLEASE DO NOT MODIFY ANYTHING IN SITU AFTER CLONING THIS REPOSITORY, AS ALL CHANGES YOU MAKE WILL BE LOST WHEN YOU NEXT `git pull`! Instead, please copy files to your own directory as and when you need to.

Those who are not using git can download the whole repository by using the [Downloads link](https://bitbucket.org/mhasoba/silbiocompmasterepo/downloads)

The Solutions directory will be updated with answers to exercises/precticals once the submission deadline has passed.

### FOR LECTURERS: 

*YOU WILL HAVE BOTH READ AND WRITE PERMISSIONS -- YOU CAN CHOOSE TO PUT YOUR LECTURES, CODE AND DATA EITHER IN THE MAIN CODE AND DATA DIRECTORIES, OR PUT THEM IN A WEEKLY DIRECTORY.

Created by Samraat Pawar spawar@imperial.ac.uk
