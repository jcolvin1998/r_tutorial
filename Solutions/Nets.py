#!/usr/bin/env python

""" 
Constructs and plots a snapshot of a collaboration network 
between Universities and partnerns, and save it as a .svg file.

"""
import csv
import networkx as nx
import scipy as sc
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches


#######################################
### Import Data #######################

### Add nodes

with open("../Data/QMEE_Net_Mat_nodes.csv") as n:
	n = csv.reader(n)
	headers = next(n)
	nodes = [row for row in n]

nodes_lists = list(set([row[0] for row in nodes]))

### Add links

with open("../Data/QMEE_Net_Mat_edges.csv", 'r') as l:
	l = csv.reader(l)
	headers2 = next(l)
	links = [row for row in l]

# Arrange edges in a directory as {(node1, node2) : weight}
k = 0
edges = {} #main links directory
for line in links: #scan per line and the column to arrange connections
	ids1 = nodes[k][0]
	m = 0
	for w in line:
		ids2 = nodes[m][0]
		A = (ids1, ids2)
		edges[A]= w
		m= m+1
	k= k+1	

## delete 0 values(no link) from the edges dictionary

x=0
no_links = [] #create a no_link list with all the couples with no connection
for y in edges.values(): 
	if y == '0':
		B = edges.keys()[x]
		no_links.append(B)
		#print "No link found"
	x=x+1

for item1 in no_links: #delete items in the no link list
	del edges[item1]


#########################################
### Arrange the Graph ###################

## Create a directed Network graph
G = nx.DiGraph()

## Add the nodes
G.add_nodes_from(nodes_lists)

## Add the edges

# change edges format to feed in the weighted edges function 
# (needs tuples with format (node1, node2, weight))
wtup = [(v, k) for k, v in edges.iteritems()]
wtupr = [ (y,k,float(v)/10) for v, (k,y) in wtup] # a kind of standardization of the weights for more readable graph

G.add_weighted_edges_from(wtupr, color='grey')# Add the edges
	
# Define edges for the graph
Myedges = G.edges()
Ecolors = [G[u][v]['color'] for u,v in edges]
Eweights = [G[u][v]['weight'] for u,v in edges]


## Colors for the nodes based on type of partner

legend=[]
custom_node_color={}
cols = ['b', 'b', 'lime', 'lime', 'lime', 'r']
c = 0
for types in (i[1] for i in nodes):
	ids = nodes[c][0]
	legend.append(types)
	custom_node_color[ids] = cols[c]
	c = c+1


################################
### Draw the plot ##############

#choose a circular form for the network
pos = nx.circular_layout(G)

#Draw
nx.draw(G, pos, arrows=True, edges=Myedges, edge_color=Ecolors, width=Eweights, 
 node_list = custom_node_color.keys(), node_color=custom_node_color.values(), node_size= 4000, with_labels=True)


## Add 3 type of nodes in legend
red_nodes = mpatches.Patch(color='r', label='Non-Hosting Partners', capstyle = 'round')
blue_nodes = mpatches.Patch(color='b', label='University',  capstyle = 'round')
green_nodes = mpatches.Patch(color='lime', label='Hosting Partner', capstyle = 'round')

# Add the legend
plt.legend(handles=[green_nodes,red_nodes,blue_nodes], loc = 1, fontsize = 10, frameon =False)

#Save the plot
plt.savefig('../Results/QMEENetPython.svg', dpi=1000)

plt.show(G) #unhash to automatically show the plot when finished
